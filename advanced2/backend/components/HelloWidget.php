<?php
namespace backend\components;
use yii\base\Widget;
use yii\helpers\Html;
class HelloWidget extends Widget{
    //public $message;
    public function init(){
        parent::init();
        ob_start();
        //echo "这是init方法里的";
    }
    public function run(){
        $content=ob_get_clean();
        echo $content;
    }
}