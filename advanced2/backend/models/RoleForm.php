<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class RoleForm extends Model
{
    public $name;
    public $isNewRecord;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['name', 'required','message'=>'不能为空'],
        ];
    }
    public function attributeLabels(){
        return [
           'name'=>'角色名称：'
        ];
    }
}
