<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
class PhotoForm extends Model
{
    public $photo_name;
    public $photo_man;
    public $imageFile;
    public $rememberMe = false;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['photo_name', 'photo_man'], 'required','message'=>'不能为空'],

            [['imageFile'], 'file', 'skipOnEmpty' => false, 'maxSize'=>1024*1024*10, 'extensions' => 'png, jpg'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
           
        ];
    }
     public function upload()
    {
            //将文件移动到我的目录
            $path = 'uploads/' .time().'0' . $this->imageFile->extension;
            $res = $this->imageFile->saveAs($path);
            if($res)
                return $path;
            else
                return false;
    }
}
?>