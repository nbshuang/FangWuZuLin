<?php
namespace backend\models;

use Yii;
use backend\models\User;
use yii\base\Model;

/**
 * Login form
 */
class EmailForm extends Model
{
   public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['email', 'required','message'=>'不能为空'],
            ['email','email','message'=>'邮箱格式不正确'],
            ['email','checkEmail']
        ];
    }
    public function attributeLabels(){
        return [
           'email'=>'电子邮箱：'
        ];
    }
    public function checkEmail(){
        if(!$this->hasErrors()){
            $user = $this->getEmail();
            if (!($user===$this->email)) {
                $this->addError($this->email, '邮箱不是自己的邮箱哦~');
            }
        }
    }
    public function getEmail(){
       $obj=User::findOne(Yii::$app->user->identity->getId());
        return $obj->email;
    }
}
