<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SystemForm extends Model
{
   public $adminEmail;
   public $adminName;
   public $adminTel;
   public $adminCopy;
   public $adminIcp;
   public $adminAddress;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['adminName','adminEmail','adminTel','adminCopy','adminIcp','adminAddress'], 'required','message'=>'不能为空'],
            ['adminEmail','email','message'=>'邮箱格式不正确']
        ];
    }
    public function attributeLabels(){
        return [
           'adminEmail'=>'网站联系邮箱：',
            'adminName'=>'联系人：',
            'adminTel'=>'联系电话：',
            'adminCopy'=>'网站版权：',
            'adminIcp'=>'备案ICP：',
            'adminAddress'=>'地址：'
        ];
    }
}
