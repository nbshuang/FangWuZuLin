<?php
namespace backend\models;

use Yii;
use yii\base\Model;
class PasswordForm extends Model{
    public $username;
    public $password;
    public $password2;
    public function rules(){
        return [
            [['username','password','password2'],'required','message'=>'不能为空'],
            ['password2','compare','compareAttribute'=>'password','message'=>'两次密码不一致']
        ];
    }
    public function attributeLabels(){
        return [
            'username'=>'管理员：',
            'password'=>'密码：',
            'password2'=>'确认密码：'
        ];
    }
}