<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title='密码重置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-password">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'password-form',
                'action'=>'?r=home/password-update'
            ]); ?>
            <input type="hidden" name="id" value="<?=$data['id'];?>">
            <?=$form->field($model,'username')->textInput(['value'=>$data['username'],'disabled'=>'disabled'])?>
            <?=$form->field($model,'password')->passwordInput(['placeholder'=>'请填写密码'])?>
            <?=$form->field($model,'password2')->passwordInput(['placeholder'=>'请再次填写密码'])?>
            <div class="form-group">
                <?= Html::submitButton('修改', ['class' => 'btn btn-primary', 'name' => 'password-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
