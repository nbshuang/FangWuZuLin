<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title='邮件请求';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="home-email">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'email-form',
            ]); ?>
            <input type="hidden" name="id" value="<?=$id;?>">
            <?=$form->field($model,'email')->textInput(['placeholder'=>'请输入您正确邮箱'])?>
            <div class="form-group">
                <?= Html::submitButton('发送', ['class' => 'btn btn-primary', 'name' => 'password-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>