<?php
$this->title='后台主页';
?>
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font">&#xe06b;</i><span>欢迎来到房屋租赁</span></div>
    </div>
    <div class="result-wrap">

    </div>
    <div class="result-wrap">
        <div class="result-title">
            <h1>网站站长信息</h1>
        </div>
        <div class="result-content">
            <ul class="sys-info-list">
                <li>
                    <label class="res-lab">网站联系邮箱：</label><span class="res-info"><?=Yii::$app->params['adminEmail']?></span>
                </li>
                <li>
                    <label class="res-lab">联系人：</label><span class="res-info"><?=Yii::$app->params['adminName']?></span>
                </li>
                <li>
                    <label class="res-lab">联系电话：</label><span class="res-info"><?=Yii::$app->params['adminTel']?></span>
                </li>
            </ul>
        </div>
    </div>
    <div class="result-wrap">
        <div class="result-title">
            <h1>版权信息</h1>
        </div>
        <div class="result-content">
            <ul class="sys-info-list">
                <li>
                    <label class="res-lab">网站版权：</label><span class="res-info">RC&copy;<?=Yii::$app->params['adminCopy']?></span>
                </li>
                <li>
                    <label class="res-lab">备案ICP：</label><span class="res-info"><?=Yii::$app->params['adminIcp']?></span>
                </li>
                <li>
                    <label class="res-lab">地址：</label><span class="res-info"><?=Yii::$app->params['adminAddress']?></span>
                </li>
            </ul>
        </div>
    </div>
</div>