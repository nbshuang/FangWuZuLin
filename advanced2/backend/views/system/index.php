<?php
use yii\helpers\Url;
use yii\helpers\Html;
use backend\assets\RoleAsset;
use yii\bootstrap\ActiveForm;
$this->title='权限管理';
$this->params['breadcrumbs'][] = $this->title;
RoleAsset::register($this);
?>
<div class="main-wrap">
    <ul class="nav nav-tabs" style="height: 50px">
        <li class="active"><a href="#role_list" data-toggle="tab">角色列表</a></li>
        <li><a href="#role_add" data-toggle="tab">角色添加</a></li>
        <li><a href="#node_add" data-toggle="tab">权限添加</a></li>
        <li><a href="#node_list" data-toggle="tab">权限列表</a></li>
    </ul>
    <div class="result-content tab-content" id="myTabContent">
    <div class="tab-pane fade" id="role_list">
        <?=$this->render('role_list')?>
    </div>
    <div class="tab-pane fade" id="role_add">
        <?=$this->render('role_form',['model'=>$modelRole])?>
    </div>
    <div class="tab-pane fade" id="node_add">
        <?=$this->render('permission_form',['model'=>$modelPermission]);?>
    </div>
    <div class="tab-pane fade" id="node_list">
        <?=$this->render('permission_list');?>
    </div>

</div>
</div>

