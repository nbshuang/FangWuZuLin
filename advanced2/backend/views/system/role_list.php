<?php
/**
 * Created by PhpStorm.
 * User: 17985
 * Date: 2017/9/13
 * Time: 15:37
 */
use yii\db\Query;
use yii\helpers\Url;
$data=(new Query())
    ->select('*')
    ->from('auth_item')
    ->where(['type'=>1])
    ->all();
?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>角色</th>
            <th>拥有权限</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
      <?php foreach($data as $v){?>
        <tr>
            <td><?=$v['name']?></td>
            <td>
                <?php
                $str='';
                $sign='';
                $arr=(new Query())
                        ->select('child')
                        ->from('auth_item_child')
                        ->where(['parent'=>$v['name']])
                        ->all();
                        foreach($arr as $val){
                            $str.=$sign.$val['child'];
                            $sign=',';
                        }
                echo $str;
                ?>
            </td>
            <td><a href="<?=Url::toRoute(['system/add-child','name'=>$v['name']])?>">赋权</a> | <a href="<?=Url::toRoute(['system/role-del','name'=>$v['name']])?>">删除</a></td>
        </tr>
        <?php }?>
        </tbody>
    </table>

