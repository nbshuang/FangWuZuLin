<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title='系统设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-update">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'system-form',
            ]); ?>
            <?php $model->adminEmail=Yii::$app->params['adminEmail']?>
            <?=$form->field($model,'adminEmail')->textInput(['placeholder'=>'请输入您正确邮箱'])?>
            <?php $model->adminName=Yii::$app->params['adminName']?>
            <?=$form->field($model,'adminName')->textInput(['placeholder'=>'请输入您正确联系人'])?>
            <?php $model->adminTel=Yii::$app->params['adminTel']?>
            <?=$form->field($model,'adminTel')->textInput(['placeholder'=>'请输入您正确联系电话'])?>
            <?php $model->adminCopy=Yii::$app->params['adminCopy']?>
            <?=$form->field($model,'adminCopy')->textInput(['placeholder'=>'请输入您正确版权信息'])?>
            <?php $model->adminIcp=Yii::$app->params['adminIcp']?>
            <?=$form->field($model,'adminIcp')->textInput(['placeholder'=>'请输入您正确备案信息'])?>
            <?php $model->adminAddress=Yii::$app->params['adminAddress']?>
            <?=$form->field($model,'adminAddress')->textInput(['placeholder'=>'请输入您正确地址'])?>
            <div class="form-group">
                <?= Html::submitButton('配置', ['class' => 'btn btn-primary', 'name' => 'system-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
