<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\db\Query;
/* @var $this yii\web\View */
/* @var $model app\models\Igoods */
/* @var $form yii\widgets\ActiveForm */
$this->title='角色赋权';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-wrap">
<div class="role-permission-form">
    <?php $form = ActiveForm::begin([
        'id'=>'RolePermissionForm',
        'action'=>Url::toRoute(['system/add-child','name'=>$model['name']])
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['disabled'=>'disabled']) ?>
    <?php foreach($data as $key=>$val){?>
        <?php $aa[$val['name']]=$val['name']?>
    <?php }?>
    <?php
    $array=[];
    $arr=(new Query())
        ->select('child')
        ->from('auth_item_child')
        ->where(['parent'=>$model['name']])
        ->all();
    foreach($arr as $v){
        $array[]=$v['child'];
    }
    ?>
    <?php $model->name=$array;?>
    <?=$form->field($model,'name')->checkboxList($aa)?>
    <div class="form-group">
        <?= Html::submitButton('赋权', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
    </div>
