<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Igoods */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="role-form">
    <?php $form = ActiveForm::begin([
        'id'=>'RoleForm',
        'action'=>'index.php?r=system/role'
    ]); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
