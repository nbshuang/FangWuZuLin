<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Igoods */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="permission-form">
    <?php $form = ActiveForm::begin([
        'id'=>'PermissionForm',
        'action'=>'index.php?r=system/permission'
    ]); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '修改', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
