<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\rbac\Item;
use yii\rbac\Rule;
use backend\models\RoleForm;
use backend\models\PermissionForm;
use backend\models\SystemForm;
use backend\models\Auth;
/**
 * Site controller
 */
class SystemController extends Controller
{
    public $layout='main2';
    public function actionUpdate(){
        $model=new SystemForm();
        if(Yii::$app->request->isPost){
            $arr=Yii::$app->request->post('SystemForm');
            $path=$_SERVER['DOCUMENT_ROOT'].'/../config/params.php';
            $config=var_export(array_merge(include($path),$arr),true);
            $str="<?php \n return ";
            $config=$str.$config.";";
            file_put_contents($path,$config);
        }
        return $this->render('update',['model'=>$model]);
    }
    public function actionRbac(){
        $modelRole=new RoleForm();
        $modelRole->isNewRecord='添加';
        $modelPermission=new PermissionForm();
        $modelPermission->isNewRecord="添加";
        return $this->render('index',['modelRole'=>$modelRole,'modelPermission'=>$modelPermission]);
    }
    public function actionRole(){
        $auth=Yii::$app->authManager;
        if(Yii::$app->request->isPost){
            $data=Yii::$app->request->post('RoleForm');
            $author = $auth->createRole($data['name']);
            $author->description='创建了'.$data['name'].'的角色';
            $auth->add($author);
        }
        return $this->redirect(['rbac']);
    }
    public function actionPermission(){
        $auth=Yii::$app->authManager;
        if(Yii::$app->request->isPost){
            $data=Yii::$app->request->post('PermissionForm');
            $author = $auth->createPermission($data['name']);
            $author->description='创建了'.$data['name'].'的权限';
            $auth->add($author);
        }
        return $this->redirect(['rbac']);
    }
    public function actionAddChild($name){
        $auth = Yii::$app->authManager;
        if(Yii::$app->request->post()){
            //如果表中存在   则删除后再赋权
            if(Yii::$app->db->createCommand('select count(1) as num from `auth_item_child` where parent=:name')->bindValue(":name",$name)->queryOne()['num']){
                Yii::$app->db->createCommand()->delete('auth_item_child','parent=:name',[':name'=>$name])->execute();
            }
            $parent = $auth->createRole($name);
            foreach(Yii::$app->request->post('Auth')['name'] as $val){
                $child = $auth->createPermission($val);
                $auth->addChild($parent,$child);
            }
            return $this->redirect(['rbac']);
        }
        return $this->render('role_permission_form',['model'=>Auth::findOne(['name'=>$name]),'data'=>Auth::findAll(['type'=>2])]);
    }
    public function actionRoleDel($name){
        if(Auth::findOne(['name'=>$name])->delete() || Yii::$app->db->createCommand()->delete('auth_item_child','parent=:name',[':name'=>$name])->execute()){
            return $this->redirect(['rbac']);
        }else{
            die("<script>alert('删除失败！');location.href='".Url::toRoute('system/rbac')."'</script>");
        }
    }
}
