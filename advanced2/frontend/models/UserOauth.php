<?php
namespace frontend\models;
use yii\db\ActiveRecord;
use frontend\models\User;
class UserOauth extends ActiveRecord{
    public static function tableName(){
        return 'user_oauth';
    }
    public function getOne($uid){
        return $this->find()->where(['open_id'=>$uid])->asArray()->one();
    }
    public function getLoginOne(){
        return $this->hasOne(User::className(),['id'=>'user_id'])->asArray();
    }
}