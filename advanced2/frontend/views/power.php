<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-bunding',
    'action'=>'?r=login/bunding&oauthUser='.$oauthUser
]); ?>
<?= $form->field($model, 'username')->textInput() ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<div class="form-group">
    <?= Html::submitButton('绑定', ['class'=>'btn', 'name' => 'bunding-button']) ?>
    <a href="?r=login/index#contact">没账号？go to 注册</a> |
    <a href="?r=login/bunding-no" class="btn">算了，直接进吧！</a>
</div>

<?php ActiveForm::end(); ?>
