<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-login',
    'action'=>'?r=login/login'
]); ?>
<?= $form->field($modelTwo, 'username')->textInput() ?>
<?= $form->field($modelTwo, 'password')->passwordInput() ?>
<?= $form->field($modelTwo,'verifyCode')->widget(yii\captcha\Captcha::className())?>
<?= $form->field($modelTwo, 'rememberMe')->checkbox() ?>
<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-paper-plane"></i>登录', ['class'=>'btn', 'name' => 'login-button']) ?>
    <a href="?r=login/oauth&oauth=1"><img src="weibo_login.png" title="点击进入授权页面" alt="点击进入授权页面" border="0" /></a>
</div>

<?php ActiveForm::end(); ?>
