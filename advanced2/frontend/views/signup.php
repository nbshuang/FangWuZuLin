<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'action'=>'?r=login/signup'
]); ?>
<?= $form->field($modelOne, 'username')->textInput() ?>

<?= $form->field($modelOne, 'email')->textInput() ?>

<?= $form->field($modelOne, 'password')->passwordInput() ?>

<div class="form-group">
    <?= Html::submitButton('<i class="fa fa-paper-plane"></i>注册', ['class'=>'btn btn-primary', 'name' => 'signup-button']) ?>
</div>

<?php ActiveForm::end(); ?>
